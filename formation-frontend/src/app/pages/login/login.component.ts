import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/security/services/authentication.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = "";
  password: string = "";
  errorMessage: string = "";

  constructor(
    private as: AuthenticationService,
    private router: Router) { }

  ngOnInit(): void {
    console.log(environment.backendUrl);
  }

  onSubmit() {
    this.as.login(this.username, this.password).subscribe({
      next: res => this.router.navigateByUrl("/"),
      error: err => this.errorMessage = "invalid username or password"
    });
  }

}
