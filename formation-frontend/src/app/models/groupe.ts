import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";

export interface Groupe {
  id: number;
  nom: string;
  nbStagiaires: number;
}


export function groupeForm(g?: Groupe): FormGroup {
  return new FormGroup({
    nom: new FormControl( g ? g.nom : "", [Validators.required, Validators.minLength(3)]),
    stagiaires : new FormArray([])
  })
}
