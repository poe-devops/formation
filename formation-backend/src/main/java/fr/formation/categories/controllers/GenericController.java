package fr.formation.categories.controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.categories.models.HasId;
import fr.formation.categories.services.GenericService;

public abstract class GenericController<DTO extends HasId<Integer>, SERVICE extends GenericService<?, ?, ?, DTO>> {

	@Autowired
	protected SERVICE service;

	@GetMapping("")
	public Collection<DTO> findAll() {
		return service.findAll();
	}

	@GetMapping("{id}")
	public DTO findById(@PathVariable int id) {
		return service.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "entity with id " + id + " does not exists"));
	}
	
	@PostMapping("")
	public DTO save(@Valid @RequestBody DTO dto) {
		dto.setId(0);
		return service.save(dto);
	}
	
	@PutMapping("{id}")
	public void update(@PathVariable int id, @Valid @RequestBody DTO dto) {
		if (dto.getId() == 0) {
			dto.setId(id);
		} else if (dto.getId() != id) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ids in url and body do not match");
		}
		service.update(dto);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable int id) {
		service.deleteById(id);
	}
	
}
