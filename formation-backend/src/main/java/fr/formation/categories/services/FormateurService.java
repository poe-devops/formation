package fr.formation.categories.services;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.formation.categories.dto.FormateurDto;
import fr.formation.categories.mappers.FormateurMapper;
import fr.formation.categories.models.Formateur;
import fr.formation.categories.repositories.FormateurRepository;

@Service
public class FormateurService extends GenericService<FormateurMapper, FormateurRepository, Formateur, FormateurDto> {

	public Collection<FormateurDto> search(String q, Pageable pageable) {
		return repository.search(q, pageable).stream()
				.map(g -> mapper.modelToDto(g))
				.collect(Collectors.toList());
	}
}
