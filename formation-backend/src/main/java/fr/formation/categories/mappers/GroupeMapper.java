package fr.formation.categories.mappers;

import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import fr.formation.categories.dto.GroupeDto;
import fr.formation.categories.models.Groupe;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface GroupeMapper extends fr.formation.categories.mappers.Mapper<Groupe, GroupeDto> {


	@Mappings({
	})
	GroupeDto modelToDto(Groupe source);

	@AfterMapping
	default void computeNombreAndMoyenne(Groupe src, @MappingTarget GroupeDto dto) {
		dto.setNbStagiaires(src.getStagiaires().size());
	}
}
