package fr.formation.categories.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.formation.categories.models.Formateur;

@Repository
public interface FormateurRepository extends JpaRepository<Formateur, Integer> {

	@Query("from Formateur f where f.prenom like %:q% or f.nom like %:q% or f.email like %:q%")
	public Collection<Formateur> search(String q);

	@Query("from Formateur f where f.prenom like %:q% or f.nom like %:q% or f.email like %:q%")
	public List<Formateur> search(String q, Pageable page);

}
