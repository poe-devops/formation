package fr.formation.categories.models;

public enum Role {
	STAGIAIRE,
	FORMATEUR,
	ADMIN
}
