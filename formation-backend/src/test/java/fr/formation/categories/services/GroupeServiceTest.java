package fr.formation.categories.services;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import fr.formation.categories.models.Groupe;
import fr.formation.categories.models.Stagiaire;

class GroupeServiceTest extends ServiceTestBase {
	
	@Autowired
	private GroupeService gs;
	
	@Test
	void addStagiaire_success() {
		// arrange
		Groupe g = Groupe.builder().nom("cursus test").id(123).build();
		when(gr.findById(123)).thenReturn(Optional.of(g));
		Stagiaire s = Stagiaire.builder().prenom("francine").id(321).build();
		when(sr.findById(321)).thenReturn(Optional.of(s));
		// act
		gs.addStagiaire(321, 123);
		// assert
		assertTrue(g.getStagiaires().contains(s));
		verify(gr, times(1)).save(g);
	}

	@Test
	void addStagiaire_errorStagiaireNotFound() {
		// arrange
		Groupe g = Groupe.builder().nom("cursus test").id(123).build();
		when(gr.findById(123)).thenReturn(Optional.of(g));
		when(sr.findById(321)).thenReturn(Optional.empty());
		// act
		assertThrows(NoSuchElementException.class, () -> gs.addStagiaire(321, 123));
		// assert
	}

}
