package fr.formation.categories.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import fr.formation.categories.services.FormateurService;
import fr.formation.categories.services.GroupeService;
import fr.formation.categories.services.ModuleService;
import fr.formation.categories.services.ParticipationService;
import fr.formation.categories.services.ServiceTestBase;
import fr.formation.categories.services.StagiaireService;

@SpringBootTest
@EnableAutoConfiguration(exclude= {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
@AutoConfigureMockMvc
class ControllerTestBase extends ServiceTestBase {

	@MockBean
	protected FormateurService fs;

	@MockBean
	protected GroupeService gs;

	@MockBean
	protected ModuleService ms;

	@MockBean
	protected ParticipationService pas;
	
	@MockBean
	protected StagiaireService ss;

	@Autowired
	protected MockMvc mvc;

}
